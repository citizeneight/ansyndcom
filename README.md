AnSyndCom
===

"Listen - strange women lying in ponds distributing swords is no basis for a
system of server administration. Supreme executive power derives from a mandate
from the masses, not from some farcical aquatic ceremony."

This project aims to use standard linux server infrastructure (users, groups, ssh,
public key crypto, etc) to faciliate the operation and administration of a server.
It's like a DAO in concept, but instead of using web3 technologies, it uses a much
simpler set of tools for managing proposal, voting, discussion, and rotating roles.

In the case of a group that already maintains a server for their group's purpose 
(web development, game development, rabble rousing, etc), members are already
using ssh + keys + terminal, so there is potentially less to learn.

## Possible features
* initial set of rules and bylaws represented in plain prose. 
* corresponding set of rules codified in code. Members agree that prose matches the code via a vote
* proposals will have a vote-by date and a vote-type (simple majority, 2/3rds maj., etc)
* rotating roles will be managed by nix groups.
* use internal mail to facilate proposals 

## use case
Game dev syndicate with 10-20 members.
initially non-admins will shadow working admins on a rotating basis. So
perhaps every week a non-admin member who wishes to become an admin will
mentor with current admins. It will be up to a vote to grant them powers
over specific functions, like updating packages or database backups.

Rouge admins will be kept in check by votes of no-confidence which can strip
that user of admin powers. the specifics of this procedure will stored 
as code and prose.
